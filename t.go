package main

import (
	"fmt"
	"bufio"
	"os"
	"strconv"
	"math/rand"
)

type Board struct {
	fields[][]string
}

type IPlayer interface {
	GetName() string
	GetInput() int
}

func isNumeric(s string) bool {
	_, err := strconv.ParseFloat(s, 64)

    return err == nil
}


//player-----------------------------------------------------------------
type Player struct {
	name string
}

func (p Player) GetInput() int {

	fmt.Printf("Gracz %s Podaj pole:", currentPlayer.GetName())
	var chosen = false
	var chosenField int

	for !chosen {
		reader := bufio.NewReader(os.Stdin)
		input, _ := reader.ReadString('\n')
		input = input[:len(input)-1]

		if(isNumeric(input)) {
			chosenField, _ = strconv.Atoi(input)
			if(!(chosenField >= 1 && chosenField <= 9)) {
				fmt.Printf("Podano niedozwoloną wartość, wybierz liczbę od 1 do 9")
				continue
			}
		} else {
			fmt.Printf("Podano niedozwoloną wartość, wybierz liczbę od 1 do 9")
				continue
		}

		var chosenBoardField = GetBoardFieldByNumber(chosenField)
		if chosenBoardField != fmt.Sprintf("  %s  ", player.GetName()) && chosenBoardField != fmt.Sprintf("  %s  ", computer.GetName()) {
			break;
		} else {
			fmt.Printf("Wybrane pole  jest już zajęte, podaj inne")
			continue
		}
	}
	fmt.Printf("chosen: %d", chosenField)
	return chosenField
}

func (p Player) GetName() string {
	return p.name
}


//computer -----------------------------------------------
type Computer struct {
	name string
}

func (c Computer) GetInput() int {
	fmt.Printf("Gracz %s :", currentPlayer.GetName())
	var chosen = false
	var chosenField int
	for !chosen {
		chosenField = rand.Intn(10 - 1) + 1
		var chosenBoardField = GetBoardFieldByNumber(chosenField)
		if chosenBoardField != fmt.Sprintf("  %s  ", player.GetName()) && chosenBoardField != fmt.Sprintf("  %s  ", computer.GetName()) {
			chosen = true
		} else {
			continue
		}
	}

	return chosenField
}

func (c Computer) GetName() string {
	return c.name
}


var isGameFinished = false
var player = Player{"X"}
var computer = Computer{"O"}
var currentPlayer IPlayer
var board = InitBoard()

func InitBoard() Board {
	currentPlayer = computer
	var board = Board{}
	row1 := []string{"1    ", "|", "2    ", "|", "3    "}
	row2 := []string{"-----", "+", "-----", "+", "-----"}
	row3 := []string{"4    ", "|", "5    ", "|", "6    "}
	row4 := []string{"-----", "+", "-----", "+", "-----"}
	row5 := []string{"7    ", "|", "8    ", "|", "9    "}
	board.fields = append(board.fields, row1, row2, row3, row4, row5)
	return board
}

func DrawBoard() {
	print("\033[H\033[2J")

	for i := 0; i < 5; i++ {
		for j := 0; j < 5; j++ {
			fmt.Print(board.fields[i][j])
		}
		fmt.Print("\n")
	}
}

func SetPlayerField(chosenField int) {
	var boardField *string
	switch chosenField {
		case 1:
			boardField = &board.fields[0][0]
		case 2:
			boardField = &board.fields[0][2]
		case 3:
			boardField = &board.fields[0][4]
		case 4:
			boardField = &board.fields[2][0]
		case 5:
			boardField = &board.fields[2][2]
		case 6:
			boardField = &board.fields[2][4]
		case 7:
			boardField = &board.fields[4][0]
		case 8:
			boardField = &board.fields[4][2]
		case 9:
			boardField = &board.fields[4][4]
		default:
			boardField = nil
	}

	if boardField != nil {
		*boardField = fmt.Sprintf("  %s  ", currentPlayer.GetName())
	}
}

func CheckWin() {
	if (
		(board.fields[0][0] == board.fields[0][2] && board.fields[0][0] == board.fields[0][4]) ||
		(board.fields[2][0] == board.fields[2][2] && board.fields[2][0] == board.fields[2][4]) ||
		(board.fields[4][0] == board.fields[4][2] && board.fields[4][0] == board.fields[4][4]) ||
		
		(board.fields[0][0] == board.fields[2][0] && board.fields[0][0] == board.fields[4][0]) ||
		(board.fields[0][2] == board.fields[2][2] && board.fields[0][2] == board.fields[4][2]) ||
		(board.fields[0][4] == board.fields[2][4] && board.fields[0][4] == board.fields[4][4]) ||

		(board.fields[0][0] == board.fields[2][2] && board.fields[0][0] == board.fields[4][4]) ||
		(board.fields[0][4] == board.fields[2][2] && board.fields[0][4] == board.fields[4][0])) {
		isGameFinished = true
		fmt.Print("Wygrał gracz:", currentPlayer.GetName())
	}
}

func ChangePlayer() {
	if currentPlayer.GetName() == player.GetName() {
		currentPlayer = computer
	} else {
		currentPlayer = player
	}
}

func main() {
	DrawBoard()
	for !isGameFinished {
		ChangePlayer()
		var chosenField = currentPlayer.GetInput()
		SetPlayerField(chosenField)
		DrawBoard()
		CheckWin()
	}
}

func GetBoardFieldByNumber(chosenField int) string {
	switch chosenField {
	case 1:
		return board.fields[0][0]
	case 2:
		return board.fields[0][2]
	case 3:
		return board.fields[0][4]
	case 4:
		return board.fields[2][0]
	case 5:
		return board.fields[2][2]
	case 6:
		return board.fields[2][4]
	case 7:
		return board.fields[4][0]
	case 8:
		return board.fields[4][2]
	case 9:
		return board.fields[4][4]
	default:
		return ""
	}
}
